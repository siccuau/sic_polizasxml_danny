unit UCuentaPreg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,UCuentasCo, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.StdCtrls,
  Data.DB, FireDAC.Comp.Client;

type
  TCuentaPreg = class(TForm)
    Conexion: TFDConnection;
    btnAceptar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    txtCC: TEdit;
    lblCC: TLabel;
    procedure txtCCKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CuentaPreg: TCuentaPreg;

implementation

{$R *.dfm}

procedure TCuentaPreg.btnAceptarClick(Sender: TObject);
begin
ModalResult:=MrOk;
end;

procedure TCuentaPreg.txtCCKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  busca_cuentas : TFCuentas;
  txt : TEdit;
begin
  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtCC.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtCC.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
      lblCC.Caption:=Conexion.ExecSQLScalar('select nombre from cuentas_co where cuenta_pt=:pt',[txtCC.Text]);
    end;
  end;

  if Key=VK_RETURN then
 begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
    else
    begin
    lblCC.Caption:=Conexion.ExecSQLScalar('select nombre from cuentas_co where cuenta_pt=:pt',[txtCC.Text]);
    end;
  end;
 end;
end;

end.
