object CuadraPoliza: TCuadraPoliza
  Left = 0
  Top = 0
  Caption = 'Cuadrar Polizas'
  ClientHeight = 156
  ClientWidth = 280
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 178
    Height = 13
    Caption = 'Cuadrar polizas con cuenta contable:'
  end
  object lblCC: TLabel
    Left = 151
    Top = 38
    Width = 3
    Height = 13
    Caption = ' '
  end
  object Label2: TLabel
    Left = 24
    Top = 72
    Width = 33
    Height = 13
    Caption = 'Fecha:'
  end
  object Label3: TLabel
    Left = 28
    Top = 96
    Width = 14
    Height = 13
    Caption = 'del'
  end
  object Label4: TLabel
    Left = 136
    Top = 96
    Width = 8
    Height = 13
    Caption = 'al'
  end
  object txtCC: TEdit
    Left = 24
    Top = 35
    Width = 121
    Height = 21
    TabOrder = 0
    OnExit = txtCCExit
    OnKeyDown = txtCCKeyDown
  end
  object btnCuadrar: TButton
    Left = 96
    Top = 118
    Width = 75
    Height = 25
    Caption = 'Cuadrar'
    TabOrder = 1
    OnClick = btnCuadrarClick
  end
  object dtpIni: TDateTimePicker
    Left = 48
    Top = 91
    Width = 82
    Height = 21
    Date = 44140.000000000000000000
    Time = 0.428854444442549700
    TabOrder = 2
  end
  object dtpFin: TDateTimePicker
    Left = 150
    Top = 91
    Width = 82
    Height = 21
    Date = 44140.000000000000000000
    Time = 0.428854444442549700
    TabOrder = 3
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Password=masterkey'
      'User_Name=sysdba'
      'Database=E:\Microsip datos 2020\VET LOYA PRUEBA 2019.FDB'
      'DriverID=FB')
    LoginPrompt = False
    Left = 242
    Top = 6
  end
  object qryPolizas: TFDQuery
    Connection = Conexion
    Left = 240
    Top = 56
  end
  object qryPolizasDet: TFDQuery
    Connection = Conexion
    Left = 240
    Top = 112
  end
end
