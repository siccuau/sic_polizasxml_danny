unit Uselempresa1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.FMTBcd, Data.DB, Data.SqlExpr,
  Vcl.StdCtrls, Vcl.DBCtrls, Data.DBXFirebird, Datasnap.DBClient,
  Datasnap.Provider, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Comp.Client, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.VCLUI.Wait, UPrincipal, ULicencia;

type
  TSeleccionaEmpresa1 = class(TForm)
    Label1: TLabel;
    ListEmpresas: TDBLookupListBox;
    btnAceptar: TButton;
    btnCancelar: TButton;
    DataSource: TDataSource;
    FDConnection: TFDConnection;
    FDQuery: TFDQuery;
    FDTransaction: TFDTransaction;
    btnCambiarEmpresas: TButton;
    Label2: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure ListEmpresasKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    lic : TLicencia;
  end;

var
  SeleccionaEmpresa1: TFPrincipal;

implementation

{$R *.dfm}

procedure TSeleccionaEmpresa1.btnAceptarClick(Sender: TObject);
var
  FormPrincipal : TFPrincipal;
  valida : string;
begin
  FormPrincipal := TFPrincipal.Create(nil);
  FormPrincipal.Conexion.Params.Values['Database'] := FDConnection.Params.Values['Database'].Replace('System\config.fdb','') +'\'+ ListEmpresas.SelectedItem + '.fdb';
  FormPrincipal.Conexion.Params.Values['Server'] := FDConnection.Params.Values['Server'];
  FormPrincipal.Conexion.Params.Values['User_Name'] := FDConnection.Params.Values['User_Name'];
  FormPrincipal.Conexion.Params.Values['Password'] := FDConnection.Params.Values['Password'];
  FormPrincipal.Conexion.Params.Values['RoleName'] := 'USUARIO_MICROSIP';
  FormPrincipal.Conexion.Open;
  valida := FormPrincipal.Conexion.ExecSQLScalar('select valor from registry where nombre = ''SIC_POLIZAS''');
  valida := lic.DecryptStr(valida,lic.StringToWord('emp'));
  if  AnsiUpperCase(valida)= AnsiUpperCase(FormPrincipal.Conexion.Params.Values['Database']) then
  begin
    FormPrincipal.StatusBar1.Panels.Items[0].Text := ListEmpresas.SelectedItem;
    FormPrincipal.Visible := True;
    FormPrincipal.Show;
    Hide;
  end
  else
  begin
    showmessage('Esta empresa no esta autorizada');
    ListEmpresas.SetFocus;
  end;
end;

procedure TSeleccionaEmpresa1.btnCancelarClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TSeleccionaEmpresa1.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TSeleccionaEmpresa1.FormCreate(Sender: TObject);
begin
  lic := TLicencia.Create;
end;

procedure TSeleccionaEmpresa1.FormShow(Sender: TObject);
begin
       FDConnection.ExecSQL('EXECUTE block as'+
    ' BEGIN'+
    ' if (not exists('+
    ' select 1 from RDB$RELATION_FIELDS rf '+
    ' where rf.RDB$RELATION_NAME = ''EMPRESAS'' and rf.RDB$FIELD_NAME = ''SIC_POLIZA_AUTORIZADO''))'+
    ' then'+
    ' execute statement ''ALTER TABLE EMPRESAS ADD SIC_POLIZA_AUTORIZADO CHAR(1)'';'+
    ' END');
  FDQuery.Open;
  if FDQuery.RecordCount=0 then
   begin
     btnAceptar.Visible:=false;
     ListEmpresas.Visible:=false;
     label2.Visible:=true;
   end;
end;
procedure TSeleccionaEmpresa1.ListEmpresasKeyPress(Sender: TObject;
  var Key: Char);
begin
   if Key = #13 then btnAceptar.SetFocus;
end;

end.
