object Password: TPassword
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = 'Password'
  ClientHeight = 131
  ClientWidth = 445
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblCaption: TLabel
    Left = 8
    Top = 35
    Width = 3
    Height = 13
  end
  object txtPassword: TEdit
    Left = 228
    Top = 32
    Width = 209
    Height = 21
    PasswordChar = '*'
    TabOrder = 0
  end
  object btnAceptar: TButton
    Left = 83
    Top = 85
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 1
    OnClick = btnAceptarClick
  end
end
