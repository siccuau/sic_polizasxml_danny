object CuentaPreg: TCuentaPreg
  Left = 0
  Top = 0
  Caption = 'Cuenta contable'
  ClientHeight = 85
  ClientWidth = 288
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 159
    Height = 13
    Caption = 'Seleccionar cuenta contable para'
  end
  object Label2: TLabel
    Left = 171
    Top = 8
    Width = 31
    Height = 13
    Caption = 'Label2'
  end
  object lblCC: TLabel
    Left = 135
    Top = 30
    Width = 3
    Height = 13
    Caption = ' '
  end
  object btnAceptar: TButton
    Left = 104
    Top = 54
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 0
    OnClick = btnAceptarClick
  end
  object txtCC: TEdit
    Left = 8
    Top = 27
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
    OnKeyDown = txtCCKeyDown
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Password=masterkey'
      'User_Name=sysdba'
      'Database=E:\Microsip datos 2020\VET LOYA PRUEBA 2019.FDB'
      'DriverID=FB')
    LoginPrompt = False
    Left = 34
    Top = 173
  end
end
