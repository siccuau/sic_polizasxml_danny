object TipoCambio: TTipoCambio
  Left = 0
  Top = 0
  Caption = 'Tipo de Cambio'
  ClientHeight = 96
  ClientWidth = 359
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label34: TLabel
    Left = 32
    Top = 24
    Width = 75
    Height = 13
    Caption = 'Tipo de cambio:'
  end
  object btnBuscar: TButton
    Left = 199
    Top = 48
    Width = 96
    Height = 25
    Caption = 'Cambio Por Fecha'
    TabOrder = 0
    OnClick = btnBuscarClick
  end
  object dtpCambio: TDateTimePicker
    Left = 113
    Top = 48
    Width = 80
    Height = 25
    Date = 44137.000000000000000000
    Time = 0.463503634258813700
    TabOrder = 1
  end
  object btnGuardarCambio: TButton
    Left = 240
    Top = 19
    Width = 75
    Height = 25
    Caption = 'Guardar'
    TabOrder = 2
    OnClick = btnGuardarCambioClick
  end
  object txtTipodeCambio: TEdit
    Left = 113
    Top = 21
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Password=masterkey'
      'User_Name=sysdba'
      'Database=E:\Microsip Pruebas\VET LOYA PRUEBA 2019.FDB'
      'DriverID=FB')
    LoginPrompt = False
    Left = 323
    Top = 60
  end
end
