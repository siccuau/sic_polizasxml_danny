unit UTipoCambio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  Vcl.StdCtrls, Vcl.ComCtrls;

type
  TTipoCambio = class(TForm)
    btnBuscar: TButton;
    dtpCambio: TDateTimePicker;
    btnGuardarCambio: TButton;
    txtTipodeCambio: TEdit;
    Label34: TLabel;
    Conexion: TFDConnection;
    procedure btnGuardarCambioClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TipoCambio: TTipoCambio;

implementation

{$R *.dfm}

procedure TTipoCambio.btnBuscarClick(Sender: TObject);
var
fecha:string;
begin
fecha:=formatdatetime('mm/dd/yyyy',dtpCambio.Date);
if Conexion.ExecSQLScalar('select tipo_cambio from historia_cambiaria where fecha=:fecha',[fecha])<>null then
 begin
   txtTipodeCambio.Text:=floattostr(Conexion.ExecSQLScalar('select tipo_cambio from historia_cambiaria where fecha=:fecha',[fecha]));
 end
 else
 begin
 ShowMessage('No se encontro un cambio para este dia.');
 txtTipodeCambio.Text:='0';
 end;
end;

procedure TTipoCambio.btnGuardarCambioClick(Sender: TObject);
begin
 Conexion.ExecSQL('update sic_plantillas_config set tipo_cambio=:cambio',[strtofloat(txtTipodeCambio.Text)]);
 Conexion.Commit;
 ShowMessage('Se guardo el tipo de cambio.');
 close;
end;

procedure TTipoCambio.FormShow(Sender: TObject);
begin
if Conexion.ExecSQLScalar('select tipo_cambio from sic_plantillas_config')<>null then
  txtTipodeCambio.Text:=Conexion.ExecSQLScalar('select tipo_cambio from sic_plantillas_config');
end;

end.
