unit UEmpleados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,strutils,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.ComCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet,UCuentasCo, Vcl.DBCtrls;

type
  TFEmpleados = class(TForm)
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    txtBuscadorCuenta: TEdit;
    grdEmpleados: TDBGrid;
    btnGuardarTodos: TButton;
    qryEmpleados: TFDQuery;
    dsEmpleados: TDataSource;
    Conexion: TFDConnection;
    Label1: TLabel;
    Label2: TLabel;
    qryConceptos: TFDQuery;
    dsConceptos: TDataSource;
    cbConcepto: TDBLookupComboBox;
    procedure qryEmpleadosAfterOpen(DataSet: TDataSet);
    procedure txtCtaEmpleadoExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtBuscadorCuentaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtBuscadorCuentaExit(Sender: TObject);
    procedure btnGuardarTodosClick(Sender: TObject);
    procedure cbConceptoCloseUp(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEmpleados: TFEmpleados;

implementation

{$R *.dfm}

procedure TFEmpleados.btnGuardarTodosClick(Sender: TObject);
var cuenta_id,cuenta_padre_id,creadas,sub_cuentas:integer;
nombre,cuenta_jt,cuenta_pt,ultimos_nueve:string;
begin
//VERIFICAR QUE NO ESTE VACIA LA CUENTA
if txtBuscadorCuenta.Text='' then
   begin
   ShowMessage('La cuenta contable no puede estar vacia.');
   end
   else
   begin
    //RECORRER EMPLEADOS
     qryEmpleados.First;
     while not qryEmpleados.Eof do
     begin
     cuenta_id:=Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cpt',[txtBuscadorCuenta.Text]);
     nombre:=qryEmpleados.FieldByName('Nombre').Value;
     nombre:=copy(nombre,0,50);
     //SI ES DE ULTIMO NIVEL CREAR LA PRIMER SUBCUENTA
     if Conexion.ExecSQLScalar('select num_subcuentas from get_num_subcuentas(:ctaid)',[cuenta_id])=0 then
      begin
        if Conexion.ExecSQLScalar('select count(nombre) from get_arbol_cuentas(:ctaid,0,''N'',''N'') where nombre=:nom',[cuenta_id,nombre])=0 then
         begin
          cuenta_padre_id:=Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cpt',[txtBuscadorCuenta.Text]);
          cuenta_jt:=Conexion.ExecSQLScalar('select cuenta_jt from get_info_cuenta(:ctaid,null)',[cuenta_id]);
          cuenta_jt:=cuenta_jt+'.000000001';
           Conexion.ExecSQL('insert into cuentas_co(cuenta_id,cuenta_padre_id,cuenta_pt,cuenta_jt,subcuenta,nombre,oculta,es_prorrateo)'+
           ' values(-1,:cpid,:cpt,:cjt,1,:nom,''N'',''N'')',[cuenta_padre_id,txtBuscadorCuenta.Text+'.1',cuenta_jt,nombre]);
          Conexion.Commit;
          creadas:=creadas+1;
         end;
      end
      else //SI NO CHECAR CUANTAS HAY Y CREAR ULTIMA
      begin
              if Conexion.ExecSQLScalar('select count(nombre) from get_arbol_cuentas(:ctaid,0,''N'',''N'') where nombre=:nom',[cuenta_id,nombre])=0 then
         begin
          cuenta_padre_id:=Conexion.ExecSQLScalar('select cuenta_id from cuentas_co where cuenta_pt=:cpt',[txtBuscadorCuenta.Text]);
          cuenta_jt:=Conexion.ExecSQLScalar('select cuenta_jt from get_info_cuenta((select max(cuenta_id) from get_arbol_cuentas(:ctaid,0,''N'',''N'')),null)',[cuenta_id]);
          ultimos_nueve:=RightStr(cuenta_jt,9);
          ultimos_nueve:=inttostr(strtoint(ultimos_nueve)+1);
          ultimos_nueve:=Format('%.*d',[9, strtoint(ultimos_nueve)]);
          cuenta_jt:=copy(cuenta_jt,0,Length(cuenta_jt)-9);
          cuenta_jt:=cuenta_jt+ultimos_nueve;
          sub_cuentas:=Conexion.ExecSQLScalar('select num_subcuentas from get_num_subcuentas(:ctaid)',[cuenta_id]);
          cuenta_pt:=txtBuscadorCuenta.Text+'.'+inttostr(sub_cuentas+1);
           Conexion.ExecSQL('insert into cuentas_co(cuenta_id,cuenta_padre_id,cuenta_pt,cuenta_jt,subcuenta,nombre,oculta,es_prorrateo)'+
           ' values(-1,:cpid,:cpt,:cjt,:sub,:nom,''N'',''N'')',[cuenta_padre_id,cuenta_pt,cuenta_jt,inttostr(sub_cuentas+1),nombre]);
          Conexion.Commit;
          creadas:=creadas+1;
         end;
      end;
      qryEmpleados.Next;
     end;
      ShowMessage(inttostr(creadas)+' Subcuentas creadas.');
   end;
end;

procedure TFEmpleados.cbConceptoCloseUp(Sender: TObject);
begin
//VERIFICA SI EL CONCEPTO TIENE CUENTA CONTABLE
if Conexion.ExecSQLScalar('select count(cuenta_contable) from conceptos_no where concepto_no_id=:id',[cbConcepto.KeyValue])=0 then
   //SI NO
   begin
   txtBuscadorCuenta.Text:='';
   end
   //SI TIENE CUENTA CONTABLE
   else
   begin
   txtBuscadorCuenta.Text:=Conexion.ExecSQLScalar('select cuenta_contable from conceptos_no where concepto_no_id=:id',[cbConcepto.KeyValue]);
   end;
end;

procedure TFEmpleados.FormShow(Sender: TObject);
begin
qryConceptos.Open();
end;

procedure TFEmpleados.qryEmpleadosAfterOpen(DataSet: TDataSet);
begin
  with grdEmpleados do
  begin
    columns[0].Visible := False;
  end;
end;

procedure TFEmpleados.txtBuscadorCuentaExit(Sender: TObject);
begin

    //VERIFICAR SI EXISTE LA CUENTA
        if Conexion.ExecSQLScalar('select count(cuenta_pt) from cuentas_co where cuenta_pt=:cpt',[txtBuscadorCuenta.Text])>0 then
        //SI EXISTE
        begin
         if txtBuscadorCuenta.Text<>Conexion.ExecSQLScalar('select count(cuenta_pt) from cuentas_co where cuenta_pt=:cpt',[txtBuscadorCuenta.Text]) then
         cbConcepto.KeyValue:=null;
        end
        //SI NO
        else
        begin
         ShowMessage('Esta cuenta contable no existe.');
         txtBuscadorCuenta.Text:='';
         cbConcepto.KeyValue:=null;
        end;
end;

procedure TFEmpleados.txtBuscadorCuentaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  cuenta_id:integer;
  busca_cuentas : TFCuentas;
begin
if Key=VK_RETURN then
  begin
   if txtBuscadorCuenta.Text='' then
    begin
      ShowMessage('La cuenta no puede estar vacia.');
    end
    else
    begin
    //VERIFICAR SI EXISTE LA CUENTA
        if Conexion.ExecSQLScalar('select count(cuenta_pt) from cuentas_co where cuenta_pt=:cpt',[txtBuscadorCuenta.Text])>0 then
        //SI EXISTE
        begin
         if txtBuscadorCuenta.Text<>Conexion.ExecSQLScalar('select count(cuenta_pt) from cuentas_co where cuenta_pt=:cpt',[txtBuscadorCuenta.Text]) then
         cbConcepto.KeyValue:=null;
        end
        //SI NO
        else
        begin
         ShowMessage('Esta cuenta contable no existe.');
         txtBuscadorCuenta.Text:='';
         cbConcepto.KeyValue:=null;
        end;
    end;
  end;



  if Key=VK_F4 then
  begin
    busca_cuentas := TFCuentas.Create(nil);
    busca_cuentas.Conexion.Params := Conexion.Params;
    busca_cuentas.Conexion.Open;
    busca_cuentas.qryCuentasCo.open;
    busca_cuentas.txtBuscar.Text :=  txtBuscadorCuenta.Text;
    if busca_cuentas.ShowModal = mrOk then
    begin
      txtBuscadorCuenta.Text := busca_cuentas.qryCuentasCo.FieldByName('cuenta_pt').AsString;
    end;
  end;

end;


procedure TFEmpleados.txtCtaEmpleadoExit(Sender: TObject);
var
  txt : TEdit;
begin
  txt := Sender as Tedit;
  if txt.Text <> '' then
  begin
    if Conexion.ExecSQLScalar('SELECT count(CUENTA_ID) FROm cuentas_co where cuenta_pt='''+txt.Text+'''') = 0 then
    begin
      showmessage('Escriba correctamente la cuenta o presione F4 para realizar la busqueda.');
      txt.SelectAll;
      txt.SetFocus;
    end
  end;
end;

end.
