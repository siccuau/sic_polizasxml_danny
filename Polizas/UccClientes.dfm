object CCclientes: TCCclientes
  Left = 0
  Top = 0
  Caption = 'Cuentas contables'
  ClientHeight = 175
  ClientWidth = 333
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblCC: TLabel
    Left = 151
    Top = 38
    Width = 3
    Height = 13
    Caption = ' '
  end
  object Label1: TLabel
    Left = 24
    Top = 33
    Width = 86
    Height = 13
    Caption = 'Cuenta a asignar:'
  end
  object lblClienteProv: TLabel
    Left = 116
    Top = 8
    Width = 5
    Height = 19
    Caption = ' '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 24
    Top = 79
    Width = 35
    Height = 13
    Caption = 'Cuenta'
  end
  object lblCuenta: TLabel
    Left = 65
    Top = 79
    Width = 3
    Height = 13
    Caption = ' '
  end
  object txtCC: TEdit
    Left = 24
    Top = 52
    Width = 121
    Height = 21
    TabOrder = 0
    OnKeyDown = txtCCKeyDown
  end
  object btnAceptar: TButton
    Left = 127
    Top = 136
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object txtCC2: TEdit
    Left = 24
    Top = 98
    Width = 121
    Height = 21
    TabOrder = 2
    OnKeyDown = txtCC2KeyDown
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Password=masterkey'
      'User_Name=sysdba'
      'Database=E:\Microsip datos 2020\VET LOYA PRUEBA 2019.FDB'
      'DriverID=FB')
    LoginPrompt = False
    Left = 298
    Top = 22
  end
end
