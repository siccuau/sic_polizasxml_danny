program Polizas;

uses
  Vcl.Forms,
  UClientes in 'UClientes.pas' {FClientes},
  UPrincipal in 'UPrincipal.pas' {FPrincipal},
  UPlantillas in 'UPlantillas.pas' {FPlantillas},
  UCuentasCo in 'UCuentasCo.pas' {FCuentas},
  UPreferencias in 'UPreferencias.pas' {FConfig},
  UConexiones in 'UConexiones.pas' {Conexiones},
  UEnvVars in 'UEnvVars.pas',
  ULicenciaProv in 'ULicenciaProv.pas' {FormLicenciaProv},
  ULogin in 'ULogin.pas' {Login},
  Uselempresa1 in 'Uselempresa1.pas' {SeleccionaEmpresa1},
  ULicencia in 'ULicencia.pas',
  WbemScripting_TLB in 'WbemScripting_TLB.pas',
  Vcl.Themes,
  Vcl.Styles,
  UVariables in 'UVariables.pas' {FVariables},
  UProveedores in 'UProveedores.pas' {FProveedores},
  UCambiaEmpresas in 'UCambiaEmpresas.pas' {FCambiaEmpresas},
  ULicenciaArchivo in 'ULicenciaArchivo.pas' {FLicenciaArchivo},
  Sysutils,
  System.ioutils,
  USelEmpresasLicencia in 'USelEmpresasLicencia.pas' {FSelEmpresasLicencia},
  UFormula in 'UFormula.pas' {Formula},
  UCuentas in 'UCuentas.pas' {Cuentas},
  UEmpleados in 'UEmpleados.pas' {FEmpleados},
  Uselempresa in 'Uselempresa.pas' {SeleccionaEmpresa},
  UTipoCambio in 'UTipoCambio.pas' {TipoCambio},
  UCuadraPolizas in 'UCuadraPolizas.pas' {CuadraPoliza},
  UccClientes in 'UccClientes.pas' {CCclientes},
  UCuentaPreg in 'UCuentaPreg.pas' {CuentaPreg},
  UAdminCambiar in 'UAdminCambiar.pas' {AdminCambiar},
  UAdmin in 'UAdmin.pas' {Admin},
  UVerificar in 'UVerificar.pas' {Verificar};

{$R *.res}
var
  lic : TLicencia;
  pwd : WideString;
begin
  lic := TLicencia.Create;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Cyan Dusk');
  pwd := GetEnvironmentVariable('sic_polizas_pwd');
 // Application.CreateForm(TFLicenciaArchivo, FLicenciaArchivo);
  Application.CreateForm(TLogin, Login);
  Application.CreateForm(TFormula, Formula);
  Application.CreateForm(TCuentas, Cuentas);
  Application.CreateForm(TFEmpleados, FEmpleados);
  Application.CreateForm(TTipoCambio, TipoCambio);
  Application.CreateForm(TCuadraPoliza, CuadraPoliza);
  Application.CreateForm(TCCclientes, CCclientes);
  Application.CreateForm(TCuentaPreg, CuentaPreg);
  Application.CreateForm(TAdminCambiar, AdminCambiar);
  Application.CreateForm(TAdmin, Admin);
  Application.CreateForm(TVerificar, Verificar);
  if pwd <> '' then
  begin
    if lic.DecryptStr(pwd,lic.stringtoword('enc')).Split([';'])[1] = trim(lic.GetHddSerialNumber) then
      begin
        Application.CreateForm(TLogin, Login);
       // Application.CreateForm(TFLicenciaArchivo, FLicenciaArchivo);
    end;
  end
  else
  begin
    Application.CreateForm(TFLicenciaArchivo, FLicenciaArchivo);
  end;
 // Application.CreateForm(TFLicenciaArchivo, FLicenciaArchivo);
 // Application.CreateForm(TSeleccionaEmpresa, SeleccionaEmpresa);
  Application.Run;
end.
